#!/bin/bash
url=$(curl --silent -i -H "Accept: application/json" https://gitlab.com/api/v4/projects/20843292/releases | tr ',' '\n,' | grep "darwin" | cut -d ":" -f2- | sed -n 2p | awk -F\" '{print $2}')
echo "Downloading binary from $url"
curl -L $url --output /usr/local/bin/cursus && \
chmod 755 /usr/local/bin/cursus && \
check=$(cat ~/.bashrc | grep "cursus save")
if [ $? == 1 ]
then
    echo "export PROMPT_COMMAND=\$PROMPT_COMMAND';history 1 | cut -c 8- | cursus save'" >> ~/.bashrc
fi
source ~/.bashrc && \
echo "Installation complete!"
