package chameleon

import (
	"fmt"
	"runtime"
)

var (
	cfcolor = make(map[string]string)
)

// Chameleon struct
type Chameleon struct {
	value interface{}
	color string
	end   string
}

// Chameleon's to String Method
func (c Chameleon) String() string {
	if c.color != "" {
		return fmt.Sprintf("%s%v%s", c.color, c.value, c.end)
	}
	return fmt.Sprint("%v", c.value)
}

// Get Chameleon object's value
func (c Chameleon) Value() interface{} {
	return c.value
}

// The retun function
func ret(color string, s interface{}) Chameleon {
	if runtime.GOOS == "windows" {
		return Chameleon{value: s}
	}
	switch s.(type) {
	case Chameleon:
		return s.(Chameleon).addColor(color)
	default:
		return Chameleon{value: s, color: color, end: "\x1b[0m"}
	}
}

// Bold
func Bold(s interface{}) Chameleon {
	return ret("\x1b[1m", s)
}

// Underlined
func Underlined(s interface{}) Chameleon {
	return ret("\x1b[4m", s)
}

// Dim
func Dim(s interface{}) Chameleon {
	return ret("\x1b[2m", s)
}

// Invert
func Inverted(s interface{}) Chameleon {
	return ret("\x1b[7m", s)
}

// Hide
func Hidden(s interface{}) Chameleon {
	return ret("\x1b[8m", s)
}

// Foregroundcolors
// Black
func Black(s interface{}) Chameleon {
	return ret("\x1b[30m", s)
}

// Red
func Red(s interface{}) Chameleon {
	return ret("\x1b[31m", s)
}

// Green
func Green(s interface{}) Chameleon {
	return ret("\x1b[32m", s)
}

// Yellow
func Yellow(s interface{}) Chameleon {
	return ret("\x1b[33m", s)
}

// Blue
func Blue(s interface{}) Chameleon {
	return ret("\x1b[34m", s)
}

// Magenta
func Magenta(s interface{}) Chameleon {
	return ret("\x1b[35m", s)
}

// Cyan
func Cyan(s interface{}) Chameleon {
	return ret("\x1b[36m", s)
}

// Lightgray
func Lightgray(s interface{}) Chameleon {
	return ret("\x1b[37m", s)
}

// Darkgray
func Darkgray(s interface{}) Chameleon {
	return ret("\x1b[90m", s)
}

// Lightred
func Lightred(s interface{}) Chameleon {
	return ret("\x1b[91m", s)
}

// Lightgreen
func Lightgreen(s interface{}) Chameleon {
	return ret("\x1b[92m", s)
}

// Lightyellow
func Lightyellow(s interface{}) Chameleon {
	return ret("\x1b[93m", s)
}

// Lightblue
func Lightblue(s interface{}) Chameleon {
	return ret("\x1b[94m", s)
}

// Lightmagenta
func Lightmagenta(s interface{}) Chameleon {
	return ret("\x1b[95m", s)
}

// Lightcyan
func Lightcyan(s interface{}) Chameleon {
	return ret("\x1b[96m", s)
}

// White
func White(s interface{}) Chameleon {
	return ret("\x1b[97m", s)
}

// Backgroundcolors
// Black
func BBlack(s interface{}) Chameleon {
	return ret("\x1b[40m", s)
}

// Red
func BRed(s interface{}) Chameleon {
	return ret("\x1b[41m", s)
}

// Green
func BGreen(s interface{}) Chameleon {
	return ret("\x1b[42m", s)
}

// Yellow
func BYellow(s interface{}) Chameleon {
	return ret("\x1b[43m", s)
}

// Blue
func BBlue(s interface{}) Chameleon {
	return ret("\x1b[44m", s)
}

// Magenta
func BMagenta(s interface{}) Chameleon {
	return ret("\x1b[45m", s)
}

// Cyan
func BCyan(s interface{}) Chameleon {
	return ret("\x1b[46m", s)
}

// Lightgray
func BLightgray(s interface{}) Chameleon {
	return ret("\x1b[47m", s)
}

// Darkgray
func BDarkgray(s interface{}) Chameleon {
	return ret("\x1b[100m", s)
}

// Lightred
func BLightred(s interface{}) Chameleon {
	return ret("\x1b[101m", s)
}

// Lightgreen
func BLightgreen(s interface{}) Chameleon {
	return ret("\x1b[102m", s)
}

// Lightyellow
func BLightyellow(s interface{}) Chameleon {
	return ret("\x1b[103m", s)
}

// Lightblue
func BLightblue(s interface{}) Chameleon {
	return ret("\x1b[104m", s)
}

// Lightmagenta
func BLightmagenta(s interface{}) Chameleon {
	return ret("\x1b[105m", s)
}

// Lightcyan
func BLightcyan(s interface{}) Chameleon {
	return ret("\x1b[106m", s)
}

// White
func BWhite(s interface{}) Chameleon {
	return ret("\x1b[107m", s)
}

// Customcolor
// Add custom color
func AddCustomColor(colorname, color string) {
	cfcolor[colorname] = fmt.Sprintf("\033[" + color)
}

// Customcolor
func CustomColor(colorname string, s interface{}) Chameleon {
	return ret(cfcolor[colorname], s)
}

// addcolor
func (c Chameleon) addColor(color string) Chameleon {
	c.color += color
	return c
}

// bold
func (c Chameleon) Bold() Chameleon {
	return c.addColor("\x1b[1m")
}

// underlined
func (c Chameleon) Underlined() Chameleon {
	return c.addColor("\x1b[4m")
}

// dim
func (c Chameleon) Dim() Chameleon {
	return c.addColor("\x1b[2m")
}

// inverted
func (c Chameleon) Inverted() Chameleon {
	return c.addColor("\x1b[7m")
}

// hide
func (c Chameleon) Hidden() Chameleon {
	return c.addColor("\x1b[8m")
}

// black
func (c Chameleon) Black() Chameleon {
	return c.addColor("\x1b[30m")
}

// red
func (c Chameleon) Red() Chameleon {
	return c.addColor("\x1b[31m")
}

// green
func (c Chameleon) Green() Chameleon {
	return c.addColor("\x1b[32m")
}

// yellow
func (c Chameleon) Yellow() Chameleon {
	return c.addColor("\x1b[33m")
}

// blue
func (c Chameleon) Blue() Chameleon {
	return c.addColor("\x1b[34m")
}

// magenta
func (c Chameleon) Magenta() Chameleon {
	return c.addColor("\x1b[35m")
}

// cyan
func (c Chameleon) Cyan() Chameleon {
	return c.addColor("\x1b[36m")
}

// lightgray
func (c Chameleon) Lightgray() Chameleon {
	return c.addColor("\x1b[37m")
}

// darkgray
func (c Chameleon) Darkgray() Chameleon {
	return c.addColor("\x1b[90m")
}

// lightred
func (c Chameleon) Lightred() Chameleon {
	return c.addColor("\x1b[91m")
}

// lightred
func (c Chameleon) Lightgreen() Chameleon {
	return c.addColor("\x1b[92m")
}

// lightyellow
func (c Chameleon) Lightyellow() Chameleon {
	return c.addColor("\x1b[93m")
}

// lightblue
func (c Chameleon) Lightblue() Chameleon {
	return c.addColor("\x1b[94m")
}

// lightmagenta
func (c Chameleon) Lightmagenta() Chameleon {
	return c.addColor("\x1b[95m")
}

// lightcyan
func (c Chameleon) Lightcyan() Chameleon {
	return c.addColor("\x1b[96m")
}

// white
func (c Chameleon) White() Chameleon {
	return c.addColor("\x1b[97m")
}

// black
func (c Chameleon) BBlack() Chameleon {
	return c.addColor("\x1b[40m")
}

// red
func (c Chameleon) BRed() Chameleon {
	return c.addColor("\x1b[41m")
}

// green
func (c Chameleon) BGreen() Chameleon {
	return c.addColor("\x1b[42m")
}

// yellow
func (c Chameleon) BYellow() Chameleon {
	return c.addColor("\x1b[43m")
}

// blue
func (c Chameleon) BBlue() Chameleon {
	return c.addColor("\x1b[44m")
}

// magenta
func (c Chameleon) BMagenta() Chameleon {
	return c.addColor("\x1b[45m")
}

// cyan
func (c Chameleon) BCyan() Chameleon {
	return c.addColor("\x1b[46m")
}

// lightgray
func (c Chameleon) BLightgray() Chameleon {
	return c.addColor("\x1b[47m")
}

// darkgray
func (c Chameleon) BDarkgray() Chameleon {
	return c.addColor("\x1b[100m")
}

// lightred
func (c Chameleon) BLightred() Chameleon {
	return c.addColor("\x1b[101m")
}

// lightgreen
func (c Chameleon) BLightgreen() Chameleon {
	return c.addColor("\x1b[102m")
}

// lightyellow
func (c Chameleon) BLightyellow() Chameleon {
	return c.addColor("\x1b[103m")
}

// lightblue
func (c Chameleon) BLightblue() Chameleon {
	return c.addColor("\x1b[104m")
}

// lightmagenta
func (c Chameleon) BLightmagenta() Chameleon {
	return c.addColor("\x1b[105m")
}

// lightcyan
func (c Chameleon) BLightcyan() Chameleon {
	return c.addColor("\x1b[106m")
}

// white
func (c Chameleon) BWhite() Chameleon {
	return c.addColor("\x1b[107m")
}

// add custom color
func (c Chameleon) AddCustomColor(colorname, color string) {
	cfcolor[colorname] = fmt.Sprintf("\033[" + color)
}

// custom color
func (c Chameleon) CustomColor(colorname string) Chameleon {
	return c.addColor(cfcolor[colorname])
}
