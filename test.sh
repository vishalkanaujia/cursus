curl --location --request POST 'https://gitlab.com/api/v4/projects/20843292/releases' \
--header 'Content-Type: application/json' \
--header 'PRIVATE-TOKEN: EX7w5ki2kcv-18vyUeep' \
--data-raw '{
    "name": "v0.5",
    "tag_name": "v0.5",
    "description": "fix: fix make-release
Signed-off-by: Ringhofer Stefan <stefan.ringhofer@strabag.com>
",
    "assets": {
        "links": [
            {
                "name": "cursus-linux-amd64",
                "url": "https://gitlab.com",
                "filepath": "/n0r1sk/cursus/uploads/1b7096a95cbaf8d8572577149bd956fc/cursus-linux-amd64",
                "link_type": "other"
            },
            {
                "name": "cursus-darwin-amd64",
                "url": "https://gitlab.com",
                "filepath": "/n0r1sk/cursus/uploads/b5cfbc04fbf2c70bf11f8f21b1fd8e98/cursus-darwin-amd64",
                "link_type": "other"
            }
        ]
    }
}'
