package main

import (
	"bytes"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strings"
	"syscall"

	chameleon "git.app.strabag.com/dev/hostingci/gomodules/hcichameleon.git"
	"github.com/atotto/clipboard"
	log "github.com/sirupsen/logrus"
)

func help() {
	fmt.Println("Did you extend your shell?")
	fmt.Println("For " + chameleon.Lightblue("zsh").String() + ": ")
	fmt.Println("\texport PROMPT_COMMAND='history | tail -n 1 | cut -c 8- | cursus save'" +
		"\n\tprecmd() {eval \"$PROMPT_COMMAND\"}")
	fmt.Println("For " + chameleon.Lightblue("bash").String() + ": ")
	fmt.Println("\texport PROMPT_COMMAND='history 1 | cut -c 8- | cursus save'")
	os.Exit(1)
}

func contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

func pasteToConsole(command string) {
	command = strings.TrimSuffix(command, "\n")
	clipboard.WriteAll(command)
	log.Printf("The command has been pasted to your clipboard! (STRG+SHIFT+V)\n- %s", command)
}

func executeCommand(command string) {
	command = strings.TrimSuffix(command, "\n")
	log.Printf("Executing the command - " + chameleon.Green(command).String())
	_, code, err := executeSingleCommand("bash", "-c", command+" > /proc/"+fmt.Sprintf("%d", os.Getppid())+"/fd/0")
	if code != 0 && code != -1 {
		if err != nil {
			log.Errorf("%s\n\n%s %s", chameleon.Red("Command Execution Error").String(), chameleon.Lightred(fmt.Sprintf("ERROR (%d):", code)).String(), err.Error())
		}
	} else {
		if err != nil {
			log.Infof("\n%s", err.Error())
		}
	}

	log.Printf("Command executed!")
}

func executeSingleCommand(command string, arg ...string) (string, int, error) {
	cmd := exec.Command(command, arg...)
	var stderr, stdout bytes.Buffer
	cmd.Stderr = &stderr
	cmd.Stdout = &stdout
	err := cmd.Run()
	if exiterr, ok := err.(*exec.ExitError); ok {
		if status, ok := exiterr.Sys().(syscall.WaitStatus); ok {
			if status.ExitStatus() != 0 {
				if stderr.String() != "" {
					return "", status.ExitStatus(), errors.New(stderr.String())
				}
				return "", status.ExitStatus(), nil
			}
		}
	} else {
		if stderr.String() != "" {
			return "", -1, errors.New(stderr.String())
		}
		if err != nil {
			return "", -1, err
		}
	}
	return stdout.String(), 0, nil
}

func reverse(s []string) []string {
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		s[i], s[j] = s[j], s[i]
	}
	return s
}
