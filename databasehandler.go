package main

import (
	"bytes"
	"database/sql"
	"fmt"
	"os"
	"regexp"
	"strings"
	"time"

	chameleon "git.app.strabag.com/dev/hostingci/gomodules/hcichameleon.git"
)

func migrateDatabase() (bool, error) {
	path := fmt.Sprintf("%s/.historian", userHomePath)
	file := fmt.Sprintf("%s/historian.db", path)
	if _, err := os.Stat(path); !os.IsNotExist(err) {
		if _, err := os.Stat(file); !os.IsNotExist(err) {
			err := os.Rename(file, cursusDBPath)
			if err != nil {
				return false, err
			}
			err = os.Remove(file)
			if err != nil {
				return true, nil
			}
			err = os.Remove(path)
			if err != nil {
				return true, nil
			}
			return true, nil
		}
	}
	return false, nil
}

func openDatabase() error {
	var err error
	db, err = sql.Open("sqlite3", cursusDBPath)
	if err != nil {
		return err
	}
	return nil
}

func getFromDatabase(query []string) {
	searchQuery := "SELECT * FROM history"
	if len(query) != 0 {
		searchQuery += " WHERE "
		likeQuery := ""

		for i, v := range query {
			likeQuery += fmt.Sprintf("command LIKE '%%%s%%'", v)
			if i < len(query)-1 {
				likeQuery += " AND "
			}
		}

		searchQuery += likeQuery
	}
	rows, err := db.Query(searchQuery)
	if err != nil {
		help()
	}

	var id int
	var timestamp int
	var command string
	var unformatted string
	var commands []string

	for rows.Next() {
		rows.Scan(&id, &timestamp, &command)
		t := time.Unix(int64(timestamp), 0)
		unformatted = command
		if !*searchPasteFlag && !*searchExecuteFlag {
			if len(query) != 0 {
				for _, x := range query {
					r := regexp.MustCompile("(?i)" + x)
					matches := r.FindAllString(command, -1)
					umatches := []string{}
					for _, y := range matches {
						if !contains(umatches, y) {
							umatches = append(umatches, y)
							command = strings.Replace(command, y, chameleon.Lightgreen(y).String(), -1)
						}
					}
				}
			}
			fmt.Printf("[%s] %s", chameleon.Lightblue(t.Format(timestampFormat)).String(), command)
		}
		commands = append(commands, unformatted)
	}

	if *searchPasteFlag {
		commands = reverse(commands)
		pasteToConsole(commands[*searchNFlag])
	}

	if *searchExecuteFlag {
		commands = reverse(commands)
		executeCommand(commands[*searchNFlag])
	}
}

func writeToDatabase(command *bytes.Buffer, timestamp time.Time) {
	statement, err := db.Prepare("CREATE TABLE IF NOT EXISTS history (id INTEGER PRIMARY KEY AUTOINCREMENT, timestamp INTEGER, command VARCHAR)")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	statement.Exec()
	statement, _ = db.Prepare("INSERT INTO history(timestamp, command) VALUES (?, ?)")
	statement.Exec(int32(timestamp.Unix()), command.String())
}
