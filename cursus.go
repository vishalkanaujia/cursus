package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"os/user"
	"time"

	_ "github.com/mattn/go-sqlite3"
	log "github.com/sirupsen/logrus"
	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	cursusPath      = ""
	cursusDBPath    = ""
	userHomePath    = ""
	timestampFormat = "2006-01-02 15:04:05"
)

func main() {
	// configure logrus logger
	if *debugFlag || os.Getenv("debug") != "" {
		*debugFlag = true
		log.SetLevel(log.DebugLevel)
	}
	customFormatter := new(log.TextFormatter)
	customFormatter.TimestampFormat = timestampFormat
	customFormatter.FullTimestamp = true
	customFormatter.ForceColors = true
	log.SetFormatter(customFormatter)
	log.SetOutput(os.Stdout)

	// get current user
	usr, err := user.Current()
	if err != nil {
		log.Fatal(err)
	}

	// set paths
	userHomePath = usr.HomeDir
	cursusPath = fmt.Sprintf("%s/.cursus", userHomePath)
	if _, err := os.Stat(cursusPath); os.IsNotExist(err) {
		os.Mkdir(cursusPath, 0700)
	}
	cursusDBPath = fmt.Sprintf("%s/cursus.db", cursusPath)

	// check old db
	b, err := migrateDatabase()
	if err != nil {
		log.Error(err)
	}
	if b {
		log.Info("Migrated database from historian to cursus")
	}

	// open database
	err = openDatabase()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// handle commands
	kpArgs := kingpin.MustParse(app.Parse(os.Args[1:]))

	switch kpArgs {
	case "save":
		buf := &bytes.Buffer{}
		n, err := io.Copy(buf, os.Stdin)
		if err != nil {
			log.Fatal(err)
		} else if n <= 1 { // command buffer always contains '\n'
			log.Fatal("No input provided")
		}
		t := time.Now()
		if !bytes.HasPrefix(buf.Bytes(), []byte(" ")) { // Check if input is not empty
			if !bytes.Contains([]byte(buf.Bytes()), []byte("cursus")) && !bytes.HasPrefix(buf.Bytes(), []byte("his ")) && !bytes.HasPrefix(buf.Bytes(), []byte("cur ")) { // Check if command is not cursus or the alias his/cur
				writeToDatabase(buf, t)
			}
		}
	case "search":
		if len(*searchCriteriaArg) == 0 {
			getFromDatabase([]string{})
		} else {
			getFromDatabase(*searchCriteriaArg)
		}
	}
}
