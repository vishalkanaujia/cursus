#!/bin/bash
url=$(curl --silent -i -H "Accept: application/json" https://gitlab.com/api/v4/projects/20843292/releases | tr '"' '\n' | grep "$1$" | grep https | head -1)
echo "Downloading binary from $url"
curl -L $url --output /tmp/cursus && \
check=$(sudo -V &>/dev/null)
if [ $? == 0 ]
then
    sudo chmod 755 /tmp/cursus && \
    check=$(/tmp/cursus &>/dev/null)
    if [ $? == 1 ]
    then
        echo "Cursus is not compatible with this os. Please report this"
        exit 1
    fi
    sudo mv /tmp/cursus /usr/local/bin/cursus && \
    check=$(cat ~/.bashrc | grep "cursus save")
    if [ $? == 1 ]
    then
        if [ "${PROMPT_COMMAND}" == "" ]
        then
            echo "export PROMPT_COMMAND=\"history 1 | cut -c 8- | cursus save\"" >> ~/.bashrc
        else
            echo "export PROMPT_COMMAND=\"\$PROMPT_COMMAND; history 1 | cut -c 8- | cursus save\"" >> ~/.bashrc
        fi
        source ~/.bashrc && \
        echo "Installation complete!"
    else
        echo "Update complete!"
    fi
else
    chmod 755 /tmp/cursus && \
    check=$(/tmp/cursus &>/dev/null)
    if [ $? == 1 ]
    then
        echo "Cursus is not compatible with this os. Please report this"
        exit 1
    fi
    mv /tmp/cursus /usr/local/bin/cursus && \
    check=$(cat ~/.bashrc | grep "cursus save")
    if [ $? == 1 ]
    then
        if [ "${PROMPT_COMMAND}" == "" ]
        then
            echo "export PROMPT_COMMAND=\"history 1 | cut -c 8- | cursus save\"" >> ~/.bashrc
        else
            echo "export PROMPT_COMMAND=\"\$PROMPT_COMMAND; history 1 | cut -c 8- | cursus save\"" >> ~/.bashrc
        fi
        source ~/.bashrc && \
        echo "Installation complete!"
    else
        echo "Update complete!"
    fi
fi