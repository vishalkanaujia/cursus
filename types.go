package main

import (
	"database/sql"

	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	app = kingpin.New("cursus", "Stores your history and makes it searchable")

	saveCommand       = app.Command("save", "Save a command")
	searchCommand     = app.Command("search", "search for a command")
	searchCriteriaArg = searchCommand.Arg("criteria", "criteria you want to search for").Strings()
	searchPasteFlag   = searchCommand.Flag("paste", "paste the nth last command from the history to your console").Short('p').Bool()
	searchExecuteFlag = searchCommand.Flag("execute", "execute the nth last command from the history to your console").Short('e').Bool()
	searchNFlag       = searchCommand.Flag("n", "specify which command should be either pasted or executed. Order is bottom to top (default last=0)").Short('n').Default("0").Int()

	debugFlag = app.Flag("debug", "enables debug").Bool()

	db *sql.DB
)
