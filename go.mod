module cursus

go 1.15

replace git.app.strabag.com/dev/hostingci/gomodules/hcichameleon.git => ./dependencies/hcichameleon

require (
	git.app.strabag.com/dev/hostingci/gomodules/hcichameleon.git v1.0.1
	github.com/alecthomas/units v0.0.0-20201120081800-1786d5ef83d4 // indirect
	github.com/atotto/clipboard v0.1.2
	github.com/kr/text v0.2.0 // indirect
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/sirupsen/logrus v1.7.0
	golang.org/x/sys v0.0.0-20210112091331-59c308dcf3cc // indirect
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
	gopkg.in/check.v1 v1.0.0-20200902074654-038fdea0a05b // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
