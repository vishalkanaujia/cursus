![](https://gitlab.com/n0r1sk/cursus/-/raw/master/Cursus-16-9.jpg?inline=false)

# Cursus

Cursus writes your commands into an sqlite db. With cursus you can search for commands and the searched term will be highlighted if it was found.

# How does it look like?


Click on the image to see the [asciinema recording](https://asciinema.org/a/HrimMri1COsDEdMNOGksk2l2G)!

[![asciicast](https://asciinema.org/a/HrimMri1COsDEdMNOGksk2l2G.png)](https://asciinema.org/a/HrimMri1COsDEdMNOGksk2l2G)

# Quickstart

## Linux AMD64 (e.g ubuntu >=20.04)
~~~
curl -sL https://gitlab.com/n0r1sk/cursus/-/raw/master/install-linux.sh | sudo -E bash -s -- cursus-linux-amd64
~~~
## Linux AMD64-Static (e.g ubuntu <=20.04)
~~~
curl -sL https://gitlab.com/n0r1sk/cursus/-/raw/master/install-linux.sh | sudo -E bash -s -- cursus-linux-amd64-static
~~~
## Linux 386
~~~
curl -sL https://gitlab.com/n0r1sk/cursus/-/raw/master/install-linux.sh | sudo -E bash -s -- cursus-linux-386
~~~
## Linux ARM
~~~
curl -sL https://gitlab.com/n0r1sk/cursus/-/raw/master/install-linux.sh | sudo -E bash -s -- cursus-linux-arm
~~~
## Linux ARM64
~~~
curl -sL https://gitlab.com/n0r1sk/cursus/-/raw/master/install-linux.sh | sudo -E bash -s -- cursus-linux-arm64
~~~

## Darwin AMD64
- Currently not supported

# Manual installation and configuration
- Copy `cursus` from the release to /usr/local/bin/

- Edit your .bashrc and add the following to it:
  - `export PROMPT_COMMAND='history 1 | cut -c 8- | cursus save'`

- Or if you use zshell, edit .zshrc and add this to the bottom:
  - `export PROMPT_COMMAND='history | tail -n 1 | cut -c 8- | cursus save'`
  - precmd() {eval "$PROMPT_COMMAND"}

- Optional you can set this alias in your .bashrc:
  - `alias cur="cursus search"`

## Overwriting `ctrl+r` Bash shortcut
If you like, you can overwrite the default reverse history search shortcut `ctrl+r` with the following addition to the `.bashrc`:

~~~
bind -x '"\C-r":echo -n "Search for: "; read -e var; cursus search $var'
~~~

# Command-Layout
## CURSUS SAVE (used to track your history)

As the example above shows, `cursus save` is used to save your last command into the SQlite database.

## CURSUS SEARCH (search in database)
~~~
usage: cursus search [<flags>] [<criteria>...]
search for a command
Flags:
 --help Show context-sensitive help (also try --help-long and --help-man).
 --debug enables debug
 -p, --paste paste the nth last command from the history to your console
 -e, --execute execute the nth last command from the history to your console
 -n, --n=0 specify which command should be either pasted or executed. Order is bottom to top (default last=0)
Args:
 [<criteria>] criteria you want to search for
 ~~~
