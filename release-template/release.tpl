#!/bin/bash
curl --location --request POST 'https://gitlab.com/api/v4/projects/20843292/releases' \
--header 'Content-Type: application/json' \
--header 'PRIVATE-TOKEN: {{ .Env.GITLAB_TOKEN }}' \
--data-raw '{
    "name": "{{ .Env.CI_COMMIT_TAG }}",
    "tag_name": "{{ .Env.CI_COMMIT_TAG }}",
    "description": "{{ .Env.DESCRIPTION }}",
    "assets": {
        "links": [
            {
                "name": "cursus-linux-386",
                "url": "https://gitlab.com{{ .Env.LINUX_386_BIN_URL }}",
                "link_type": "package"
            },
            {
                "name": "cursus-linux-amd64",
                "url": "https://gitlab.com{{ .Env.LINUX_AMD64_BIN_URL }}",
                "link_type": "package"
            },
            {
                "name": "cursus-linux-amd64-static",
                "url": "https://gitlab.com{{ .Env.LINUX_AMD64_STATIC_BIN_URL }}",
                "link_type": "package"
            },
            {
                "name": "cursus-linux-arm",
                "url": "https://gitlab.com{{ .Env.LINUX_ARM_BIN_URL }}",
                "link_type": "package"
            },
            {
                "name": "cursus-linux-arm64",
                "url": "https://gitlab.com{{ .Env.LINUX_ARM64_BIN_URL }}",
                "link_type": "package"
            },
            {
                "name": "cursus-darwin-arm64",
                "url": "https://gitlab.com{{ .Env.DARWIN_BIN_URL }}",
                "link_type": "package"
            }
        ]
    }
}'